#include "dac.h"

volatile int state = LOW;

HardwareTimer timer2(2);
HardwareTimer timer1(1);

int pitch = 4900;
int yaw = 4900;

int toggle = 1;

int zoom_in = 0;
int zoom_out = 0;

unsigned long timer = 0;
unsigned long yaw_timer = 0;

unsigned long timeold = 0;
int pulseT = 0;
int pulse = 0;

char inByte = 0;

void setup() {
    // Set up the built-in LED pin as an output:
    //Serial1.begin(9600);
    
    pinMode(1, PWM);
    pinMode(3, PWM);
    
    //dac_init(DAC, DAC_CH1);

    pinMode(21, OUTPUT);
    pinMode(10, OUTPUT);
    
    digitalWrite(21, LOW);
    digitalWrite(10, HIGH);
    
    timer1.setPrescaleFactor(22);
    timer1.setOverflow(65535);
    
    timer2.setPrescaleFactor(22);
    timer2.setOverflow(65535);
    
    pinMode(8, INPUT);
    attachInterrupt(8, blink, CHANGE);
}

void loop() {
  
  if(pulse < 2050 && pulse > 950)
  {
    /*char inbyte = Serial1.read();
    SerialUSB.println(inbyte);*/
    
    //SerialUSB.println(pulse);
    delay(10);
    
         if (pulse > 1140 && pulse < 1160 && pitch > 3400) pitch-=2;
         else if (pulse > 1090 && pulse <1110 && pitch > 3400) pitch-=1;
         //else if (inbyte == '1') pitch = 4900;
         
         
         else if (pulse > 1040 && pulse < 1060 && pitch < 6400) pitch+=1;       
         else if (pulse > 990 && pulse < 1010 && pitch < 6400) pitch+=2;
         
         else if (pulse > 1190 && pulse < 1210) yaw = 4400;
         else if (pulse > 1240 && pulse < 1260) yaw = 4650;
         else if (pulse > 1480 && pulse < 1520) yaw = 4900;
         else if (pulse > 1290 && pulse < 1310) yaw = 5150;
         else if (pulse > 1340 && pulse < 1360) yaw = 5400;
         
         if(yaw != 4900) yaw_timer = millis();
         
     /*if(pulse > 1780 && pulse < 1820) {
       
       //dac_write_channel(DAC, DAC_CH1, 1850);
       //SerialUSB.println("OK");
       zoom_out = 1;
       zoom_in = 0;
       timer = millis();

     }
     
     else if(pulse > 1680 && pulse < 1720) {
       
      //dac_write_channel(DAC, DAC_CH1, 1375);
      zoom_in = 1;
      zoom_out = 0;
      timer = millis();
      
     }
     
     else if(pulse > 1980 && pulse < 2020) {
       
      dac_write_channel(DAC, DAC_CH1, 0);
      delay(100);
      
     }*/
     
     else if(pulse > 1780 && pulse < 1820) {
       
             digitalWrite(21, HIGH);
             digitalWrite(10, LOW);
             delay(50);
           }
             
     else if(pulse > 1680 && pulse < 1720)
           { 
             digitalWrite(21, LOW);
             digitalWrite(10, HIGH);
             delay(50);
        
      
     }
     /*else if(inbyte == 'm') {
       
       SerialUSB.println("M");
       
      dac_write_channel(DAC, DAC_CH1, 912);
      delay(200);
      
     }*/
  //delay(200);        
  }
  
  /*if(millis() - timer < 200)
  {
    if(zoom_in) dac_write_channel(DAC, DAC_CH1, 1375);
    else if(zoom_out) dac_write_channel(DAC, DAC_CH1, 1850);
  } 
   
  else 
  {
    dac_write_channel(DAC, DAC_CH1, 2500);
    zoom_in = 0;
    zoom_out = 0;
  }*/

  if(millis() - yaw_timer > 100) yaw = 4900;
    
  pwmWrite(1, yaw);
  pwmWrite(3, pitch);

}


void blink() {
    if (digitalRead(8)) {
        timeold = micros();
    } else { 
        pulseT = micros() - timeold;
        if (pulseT < 2050 && pulseT > 950)
         { 
           pulse = pulseT - 10;
           //SerialUSB.println(pulse);
         }
    }
}
