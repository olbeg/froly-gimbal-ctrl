DEVICE_TYPE = STM32F10X_HD
STARTUP_FILE = stm32f10x_hd

# Set the external clock frequency
HSE_VALUE = 8000000L

USE_STDPERIPH_DRIVER = -DUSE_STDPERIPH_DRIVER

LIBM = -lm

DEBUG = 1
export MESSAGES

TARGET_ARCH = -mcpu=cortex-m3 -mthumb

INCLUDE_DIRS = -I . -I lib -I src -I lib/cmsis -I lib/cmsis/startup -I lib/stm/inc \
			-I lib/FreeRTOS/Source/include -I lib/FreeRTOS/Source/portable

LIBRARY_DIRS = -lrdimon

DEFINES = -D$(DEVICE_TYPE) -DHSE_VALUE=$(HSE_VALUE) $(USE_STDPERIPH_DRIVER)

export DEFINES

COMPILE_OPTS = $(WARNINGS) $(TARGET_OPTS) $(MESSAGES) $(INCLUDE_DIRS) $(DEFINES)
WARNINGS = -Wall -W -Wshadow -Wcast-qual -Wwrite-strings -Winline

ifdef DEBUG
	TARGET_OPTS = -O0 -g
	DEBUG_MACRO = -DDEBUG
else
	TARGET_OPTS = -O1 $(F_INLINE) $(F_INLINE_ONCE) $(F_UNROLL_LOOPS)
	F_INLINE = -finline
	F_INLINE_ONCE = -finline-functions-called-once
endif

CC = arm-none-eabi-gcc

CFLAGS = -std=c11 $(COMPILE_OPTS) --specs=rdimon.specs

AS = $(CC) -x assembler-with-cpp -c $(TARGET_ARCH)
ASFLAGS = $(COMPILE_OPTS)

LD = $(CC)
LDFLAGS = -Wl,--gc-sections,-Map=$(MAIN_MAP),-cref -T stm32f10x_512k_64k.ld $(INCLUDE_DIRS)\ $(LIBRARY_DIRS) $(LIBM)

AR = arm-none-eabi-ar
ARFLAGS = cr

OBJCOPY = arm-none-eabi-objcopy
OBJCOPYFLAGS = -O binary

STARTUP_OBJ = lib/cmsis/startup/startup_$(STARTUP_FILE).o

MAIN_OUT = main.elf
MAIN_MAP = $(MAIN_OUT:%.elf=%.map)
MAIN_BIN = $(MAIN_OUT:%.elf=%.bin)

MAIN_OBJS = $(sort \
	$(patsubst %.c,%.o,$(wildcard *.c)) \
	$(patsubst %.s,%.o,$(wildcard *.s)) \
	$(patsubst %.c,%.o,$(wildcard lib/cmsis/*.c)) \
	$(patsubst %.c,%.o,$(wildcard lib/stm/src/*.c)) \
	$(patsubst %.c,%.o,$(wildcard lib/FreeRTOS/Source/*.c)) \
	$(patsubst %.c,%.o,$(wildcard lib/FreeRTOS/Source/portable/*.c)) \
	$(patsubst %.c,%.o,$(wildcard src/*.c)) \
	$(STARTUP_OBJ))

# all

.PHONY: all
all: $(MAIN_BIN)

# main

$(MAIN_OUT): $(MAIN_OBJS) $(FWLIB) $(NOSYSLIB) $(USBLIB)
	$(LD) $(TARGET_ARCH) $^ -o $@ $(LDFLAGS)

$(MAIN_OBJS): $(wildcard *.h) \
	$(wildcard *.h)\
	$(wildcard external/common/include/*.h)\
	$(wildcard lib/*.h)\
	$(wildcard lib/cmsis/*.h)\
	$(wildcard lib/cmsis/startup/*.h)\
	$(wildcard lib/stm/inc/*.h)\
	$(wildcard lib/FreeRTOS/Source/include/*.h)\
	$(wildcard lib/FreeRTOS/Source/portable/include/*.h)\
	$(wildcard lib/FreeRTOS/Source/portable/*.h)\
	$(wildcard src/*.h)

$(MAIN_BIN): $(MAIN_OUT)
	$(OBJCOPY) $(OBJCOPYFLAGS) $< $@

# fwlib

.PHONY: fwlib
fwlib: $(FWLIB)

# usblib

.PHONY: usblib
usblib: $(USBLIB)

$(USBLIB): $(wildcard lib/STM32_USB-FS-Device_Driver/inc*.h)
	@cd lib/STM32_USB-FS-Device_Driver && $(MAKE)

# flash

.PHONY: flash
flash: flash-elf
#flash: flash-bin

.PHONY: flash-elf
flash-elf: all
	@cp $(MAIN_OUT) jtag/flash.elf
	@cd jtag && openocd -f flash-elf.cfg
	@rm jtag/flash.elf

.PHONY: flash-bin
flash-bin: all
	@cp $(MAIN_BIN) jtag/flash.bin
	@cd jtag && openocd -f flash-bin.cfg
	@rm jtag/flash.bin

.PHONY: upload
upload: all
	@python jtag/stm32loader.py -p $(STM32LDR_PORT) -b $(STM32LDR_BAUD) \
	-e $(STM32LDR_VERIFY) -w main.bin

# clean

.PHONY: clean
clean:
	rm -f $(MAIN_OBJS) $(MAIN_OUT) $(MAIN_MAP) $(MAIN_BIN)
	rm -f lib/cmsis/startup/*.o
	rm -f lib/cmsis/*.o
	rm -f lib/stm/src/*.o
	rm -f lib/FreeRTOS/Source/*.o
	rm -f lib/FreeRTOS/Source/portable/*.o
	rm -f src/*.o
