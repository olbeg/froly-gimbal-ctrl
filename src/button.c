#include "button.h"

TimerHandle_t button_reset_timer;
void button_pins_init();
void button_push_callback();
void gpio_pin_trigger(uint16_t pin, bool enable);

void buttons_init() {
	button_pins_init();
	button_reset_timer = xTimerCreate("timer", 10000, pdFALSE, 2, &button_push_callback);
}

void button_pins_init() {
	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_13 | GPIO_Pin_14 | GPIO_Pin_15;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOC, &GPIO_InitStructure);
}

void gpio_pin_trigger(uint16_t pin, bool enable) {
	if (enable) {
		GPIO_SetBits(frolyBUTTONS_GPIO, pin);
	} else {
		GPIO_ResetBits(frolyBUTTONS_GPIO, pin);
	}
}

void button_push(uint8_t button, uint16_t msec) {
	gpio_pin_trigger(GPIO_Pin_13, button & 1);
	gpio_pin_trigger(GPIO_Pin_14, button >> 1);
	xTimerChangePeriod(button_reset_timer, msec * 10, 100);
	xTimerReset(button_reset_timer, 100);
	GPIO_SetBits(frolyBUTTONS_GPIO, GPIO_Pin_15);
}

void button_push_callback() {
	GPIO_ResetBits(frolyBUTTONS_GPIO, GPIO_Pin_15);
}
