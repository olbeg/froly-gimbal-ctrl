#pragma once
#include "common.h"

/*
_______________________
|_______|_PC13_|_PC14_|
| MODE  |  0   |  0   |
| PWR   |  0   |  1   |
| REC   |  1   |  0   |
| PHOTO |  1   |  1   |
-----------------------
*/

#define BTN_MODE    0
#define BTN_PWR     1
#define BTN_REC     2
#define BTN_PHOTO   3

// must be called before first use of button_push
void buttons_init();
// push button selected from defines above for certain period of time
void button_push(uint8_t button, uint16_t msec);
