#pragma once

#include "FreeRTOS.h"
#include "FreeRTOSConfig.h"
#include "FrolyConfig.h"

#include <stdlib.h>
#include <stdbool.h>
#include <math.h>

#include "stm32f10x.h"
#include "stm32f10x_conf.h"
#include "stm32f10x_gpio.h"

#include "task.h"
#include "queue.h"
#include "semphr.h"
#include "timers.h"
#include "event_groups.h"

#include "external/common/include/protocol.h"

extern xQueueHandle usart_queue;
