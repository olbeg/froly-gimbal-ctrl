#include "flash.h"

bool flash_memoryWrite(uint32_t *pointer, uint16_t size) {
	uint32_t page_address = PAGE_ADDRESS;
	FLASH_UnlockBank1();
	FLASH_ClearFlag(FLASH_FLAG_EOP | FLASH_FLAG_PGERR | FLASH_FLAG_WRPRTERR);
	FLASH_Status FLASHStatus = FLASH_ErasePage(PAGE_ADDRESS);
	if (FLASHStatus != FLASH_COMPLETE)
		return false;

	for (uint16_t i = 0; i < size; ++i) {
		if (FLASH_ProgramWord(page_address + (i * 4), *pointer) != FLASH_COMPLETE) {
			return false;
		}
		pointer++;
	}
	FLASH_LockBank1();
	return true;
}