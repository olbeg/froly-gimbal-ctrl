#include "common.h"
#include "uart.h"

void USART1_IRQHandler(void);

void USART1_IRQHandler(void) {
	BaseType_t xHigherPriorityTaskWoken = pdFALSE;
	uint16_t check = usart_rx(USART1);
	if (check != 0x0100) {
		uint8_t byte = check & 0x00ff;
		xQueueSendToBackFromISR(usart_queue, (void*)&byte, &xHigherPriorityTaskWoken);
		portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
	}
}