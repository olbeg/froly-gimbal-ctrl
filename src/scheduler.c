#include "scheduler.h"
#include "uart.h"
#include "flash.h"
#include "button.h"

xQueueHandle usart_queue = NULL;
xQueueHandle flash_queue = NULL;
xQueueHandle ctrl_queue = NULL;
TimerHandle_t yaw_reset_timer = NULL;

void periph_init();
void pc13_init();
void usart1_init();
void pwm_tim2_ch2_ch3_init();
void uart_handler();
void gimbal_control();
void debug();
void flash_write();
bool uart_queue_parser(uint8_t *byte, uint8_t *buffer, uint8_t length);
void yaw_reset_callback(TimerHandle_t id);

void scheduler_init() {
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_4);

	usart_queue = xQueueCreate(256, sizeof(uint8_t));
	flash_queue = xQueueCreate(1, sizeof(ctrl_message));
	ctrl_queue = xQueueCreate(1, sizeof(ctrl_message));
	yaw_reset_timer = xTimerCreate("timer", 2000, pdTRUE, 1, &yaw_reset_callback);
	periph_init();
	buttons_init();

	xTaskCreate(gimbal_control, "gimbal_control", 512, NULL, 3, NULL);
	xTaskCreate(uart_handler, "uart_handler", 512, NULL, 3, NULL);
	xTaskCreate(flash_write, "flash_write", 512, NULL, 3, NULL);

	#if (frolyDEBUG == 1)
	xTaskCreate(debug, "debug", 512, NULL, 3, NULL);
	#endif
	xTimerReset(yaw_reset_timer, 100);

	vTaskStartScheduler();
}

void periph_init() {
	SystemInit();
	RCC_APB1PeriphClockCmd (
		RCC_APB1Periph_TIM2, ENABLE);
	RCC_APB2PeriphClockCmd (
		RCC_APB2Periph_GPIOA 	|
		RCC_APB2Periph_GPIOB 	|
		RCC_APB2Periph_GPIOC 	|
		RCC_APB2Periph_GPIOD 	|
		RCC_APB2Periph_AFIO		|
		RCC_APB2Periph_USART1 	|
		RCC_APB2Periph_ADC1, ENABLE);

	usart1_init();
	pc13_init();
	pwm_tim2_ch2_ch3_init();
}

void pc13_init() {
	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_13;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOC, &GPIO_InitStructure);
}

#define UART_BUFFER_SIZE 96 // > sizeof(ctrl_message * 2)
void uart_handler() {
	uint8_t buffer[UART_BUFFER_SIZE];
	for (;;) {
		uint8_t byte = 0;
		if (xQueueReceive(usart_queue, &byte, portMAX_DELAY) == pdTRUE) {
			if (uart_queue_parser(&byte, &buffer, UART_BUFFER_SIZE)) {
				xTimerReset(yaw_reset_timer, 100);
				usart_tx(USART1, 'A');
				usart_tx(USART1, 'C');
				usart_tx(USART1, 'K');
			}
		}
		taskYIELD();
	}
}

void flash_write() {
	ctrl_message message;
	for (;;) {
		if (xQueueReceive(flash_queue, &message, portMAX_DELAY) == pdTRUE)
			flash_memoryWrite((void*)&message.head1, sizeof(ctrl_message));
	}
	vTaskDelete(NULL);
}

#if (frolyDEBUG == 1)
void debug() {
	ctrl_message message = { PACKET_HEADER_INIT(CTRL_MSG), .data.ctrl.pitch = 1, .data.ctrl.roll = 2, .data.ctrl.yaw = 3 };
	TickType_t tick = xTaskGetTickCount();

	for(;;) {
		message.timestamp = xTaskGetTickCount() / 10000.f;
		uint8_t *ptr = &message.head1;
		for (size_t i = 0; i < sizeof(ctrl_message); ++i) {
			uint8_t byte = (*(ptr++));
			xQueueSendToBack(usart_queue, (void*)&byte, portMAX_DELAY);
		}
	}
}
#endif

void gimbal_control() {
	ctrl_message *settings = (ctrl_message*)PAGE_ADDRESS;
	if (settings->head1 == HEAD_BYTE1 && settings->head2 == HEAD_BYTE2)
		;//apply settings stored in flash

	GPIO_SetBits(GPIOC, GPIO_Pin_13);
	ctrl_message message;
	for (;;) {
		if (xQueueReceive(ctrl_queue, &message, portMAX_DELAY) == pdTRUE) {
			if (message.msg_type == CTRL_MSG) {
				TIM2->CCR2 = message.data.ctrl.pitch;
				TIM2->CCR3 = message.data.ctrl.yaw;
				if (message.data.ctrl.camera == 1) {
					GPIO_SetBits(GPIOC, GPIO_Pin_13);
				}
				if (message.data.ctrl.camera == 2) {
					GPIO_ResetBits(GPIOC, GPIO_Pin_13);
				}
			}
			if (message.msg_type == SETTINGS_MSG) {
				#if (frolyENABLE_PWM_MSG == 1)
					if (message.data.settings.pwm_enable) {
						TIM_Cmd(TIM2, ENABLE);
					} else {
						TIM_Cmd(TIM2, DISABLE);
					}
				#endif
			}
		}
	}
}

bool uart_queue_parser(uint8_t *byte, uint8_t *buffer, uint8_t length) {
	static uint8_t index = 0;
	if (index >= length)
		index = 0;

	buffer[index++] = (uint8_t)*byte;
	if (index < sizeof(ctrl_message))
		return false;

	for (size_t i = 0; i < index; ++i) {
		if (buffer[i] == 0xA3 && buffer[i + 1] == 0x95) {
			if (buffer[i + 2] == CTRL_MSG || buffer[i + 2] == SETTINGS_MSG) {
				ctrl_message *message = (ctrl_message*)(&buffer[i]);
				if (message->crc == PACKET_GIMBAL_CRC(&buffer[i])) {
					xQueueSendToBack(ctrl_queue, message, portMAX_DELAY);
					index = 0;
					return true;
				} else if (index + sizeof(ctrl_message) < (sizeof(ctrl_message) * 2 + 2)) {
					return false;
				} else {
					index = 0;
					return false;
				}
			}
		}
	}
	return false;
}

void usart1_init() {
	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;
	GPIO_Init(GPIOA, &GPIO_InitStructure);
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPD;
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	USART_InitTypeDef USART_InitStructure;
	USART_InitStructure.USART_BaudRate = 115200;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	USART_Init(USART1, &USART_InitStructure);

	USART_Cmd(USART1, ENABLE);
	USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);

	NVIC_InitTypeDef NVIC_InitStructure;
	NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 5;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
}

void pwm_tim2_ch2_ch3_init() {
	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_1 | GPIO_Pin_2;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;
	TIM_OCInitTypeDef TIM_OCConfig;

	TIM_TimeBaseStructure.TIM_ClockDivision = 0;
	TIM_TimeBaseStructure.TIM_RepetitionCounter = 0;
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;

	TIM_TimeBaseStructure.TIM_Period = 1999;
	TIM_TimeBaseStructure.TIM_Prescaler = 719;
	TIM_TimeBaseInit(TIM2, &TIM_TimeBaseStructure);

	TIM_OCConfig.TIM_OCMode = TIM_OCMode_PWM1;
	TIM_OCConfig.TIM_OutputState = TIM_OutputState_Enable;
	TIM_OCConfig.TIM_OCPolarity = TIM_OCPolarity_High;
	TIM_OCConfig.TIM_Pulse = frolyNEUTRAL_PWM;

	TIM_OC2Init(TIM2, &TIM_OCConfig);
	TIM_OC2PreloadConfig(TIM2, TIM_OCPreload_Enable);
	TIM_OC3Init(TIM2, &TIM_OCConfig);
	TIM_OC3PreloadConfig(TIM2, TIM_OCPreload_Enable);

	TIM_ARRPreloadConfig(TIM2, ENABLE);
	#if (frolyENABLE_PWM_MSG == 0)
	TIM_Cmd(TIM2, ENABLE);
	#endif
}

void yaw_reset_callback(TimerHandle_t id) {
	TIM2->CCR3 = frolyNEUTRAL_PWM;
}