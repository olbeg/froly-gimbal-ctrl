#include "uart.h"

uint16_t usart_rx(USART_TypeDef* USARTx) {
	uint16_t status;
	uint8_t byte;
	status = USARTx->SR;
	while (status & (USART_FLAG_RXNE | USART_FLAG_ERRORS)) {
		byte = USARTx->DR;
		if (!(status & USART_FLAG_ERRORS)) {
			return (uint16_t)byte;
		}
		status = USARTx->SR;
	}
	return 0x0100;
}

void usart_tx(USART_TypeDef* USARTx, u8 byte) {
	USARTx->DR = ((uint16_t)byte & 0x01FF);
	while (USART_GetFlagStatus(USARTx, USART_FLAG_TC) == RESET);
}