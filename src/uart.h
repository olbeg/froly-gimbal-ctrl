#pragma once

#include "common.h"

#define USART_FLAG_ERRORS (USART_FLAG_ORE | USART_FLAG_NE | USART_FLAG_FE | USART_FLAG_PE)

uint16_t usart_rx(USART_TypeDef* USARTx);
void usart_tx(USART_TypeDef* USARTx, uint8_t byte);
